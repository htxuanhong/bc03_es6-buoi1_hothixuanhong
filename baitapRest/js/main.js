let sum = (...args) => {
  let sum = 0;

  args.forEach((arg) => (sum += arg));

  return (sum / args.length).toFixed(2);
};

document
  .getElementById("btnKhoi1")
  .addEventListener("click", function (toan, ly, hoa) {
    toan = document.getElementById("inpToan").value * 1;
    ly = document.getElementById("inpLy").value * 1;
    hoa = document.getElementById("inpHoa").value * 1;

    document.getElementById("tbKhoi1").innerText = sum(toan, ly, hoa);
  });

document
  .getElementById("btnKhoi2")
  .addEventListener("click", function (van, su, dia, english) {
    van = document.getElementById("inpVan").value * 1;
    su = document.getElementById("inpSu").value * 1;
    dia = document.getElementById("inpDia").value * 1;
    english = document.getElementById("inpEnglish").value * 1;

    document.getElementById("tbKhoi2").innerText = sum(van, su, dia, english);
  });
